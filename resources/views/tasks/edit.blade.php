@extends('layouts.app')
@section('content')


<h1> Edit task </h1>
<form method = 'post' action = "{{action('TaskController@update', $task->id)}}"  >
@method('PATCH')
@csrf
<div class = "form-group">
    <label for = "title"> Task to update </label>
    <input type = "text" class = "form-control" name = "title" value = "{{$task->title}}">
    <input type = "text" class = "form-control" name = "status" value = "{{$task->status}}">
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Update">
</div>

</form>


<!--

<form method = 'post' action = "{{action('TaskController@destroy', $task->id)}}"  >
@csrf
@method('DELETE')
<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Delete">
</div>

</form>
-->

@endsection
