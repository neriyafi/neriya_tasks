
<head>

<style>
    h1 {
      color: white;
      text-align: center;
      text-decoration: underline;
    }
          table {
            margin-top: 50px;
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }

          td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            
          }

          tr:nth-child(even) {
            background-color: #dddddd;
      
          }
          tr:nth-child(odd){
            background-color: #aaaaaa;
      
          }
          tr:nth-child(even):hover {
            background-color: gray;
      
          }
          tr:nth-child(odd):hover {
            background-color: gray;
      
          }
</style>
</head>

@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<body>

@if (Request::is('tasks'))  
<h2><a href="{{action('TaskController@mytasks')}}">click to see your Tasks</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">click to see All Tasks</a></h2>

@endif

          <table>
            <tr>
              <th>ID</th>
              <th>Title</th>
              <th>status</th>
              <th>user_id</th>
              <th>edit</th>
              @can('admin')   <th>delete</th> 
              <th>mark as done:</th> @endcan
           
            </tr>
                    

            @foreach($tasks as $task)
            <tr>
             <td>{{$task->id}}</td>
            <td> {{$task->title}}</td>  
             <td>{{$task->status}}</td>
             <td>{{$task->user_id}}</td>
             <td> <a href= "{{ route('tasks.edit', $task->id)}}"> edit </a> </td>  

            @can('admin')
             <td>   
             <form method = 'post' action = "{{action('TaskController@destroy', $task->id)}}"  >
              @csrf
              @method('DELETE')
              <div class = "form-group">
              <input type = "submit" class= "form-control" name="submit" value= "Delete">
              </div>
              </form>
             </td>    
        

            <td>
            @if ($task->status=="done")
           <!--<button id ="{{$task->id}}" value="1"> Done!</button> -->
           <h1>  Done!  </h1>
       @else
           <button style="text-decoration: underline" id ="{{$task->id}}" value="done"> Mark as done</button>
           
       @endif
             </td>  
             @endcan
            @endforeach
           </table>

 <a href= "{{ route('tasks.create')}}"> create a new task  </a>

<script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   //if i want it to be on and of
                 // data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                 data: JSON.stringify({'status':(event.target.value), _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
   </script>

</body>
   
@endsection

