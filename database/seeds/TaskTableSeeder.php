<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(
            [
                [
                    'id' => '1',
                    'title' => 'test task 1',
                    'status' => 'Done',
                    'user_id' => '1',
                    'created_at' => date('Y,m,d G:i:s'),
                    
                ],
                [
                    'id' => '2',
                    'title' => 'test task 2',
                    'status' => 'Done',
                    'user_id' => '1',
                    'created_at' => date('Y,m,d G:i:s'),
                    
                ],
                [
                    'id' => '3',
                    'title' => 'test task 3',
                    'status' => 'Open',
                    'user_id' => '1',
                    'created_at' => date('Y,m,d G:i:s'),
                    
                ],
            ]);
    }
}
